error_fig_log_t = figure;
error_fig_log_r = figure;
error_fig = figure;
err = results.err;
for i=1:9
    t = results.(['t',num2str(i)]);
    dt = t(2)-t(1);
    probe = results.(['probe',num2str(i)]);
    ndx = i;
    dx = err(2,ndx);
    %t=0:tf/nt:tf;
    f_temp1 = probe(2:end,1);
    f_temp2 = probe(2:end,3);
    [ct,indt] = max(f_temp2(t>=20&t<=30));
    [ct2,indt2] = max(f(t(t>=20&t<=30)+dt+10,5,(1/(1+0.5)),0.5,25,0.5/(2*sqrt(log(2))))');
    difft_t = t(indt2)-t(indt);
    [cr,indr] = min(f_temp1(t>=15&t<=25));
    [cr2,indr2] = min(f(t(t>=15&t<=25)+2*dt,5,((0.5-1)/(1+0.5)),1,15,1/(2*sqrt(log(2))))');
    difft_r = t(indr2)-t(indr);
    %err(1,ndx) = ndx;
    %err(2,ndx) = dx;
    %err(3,ndx) = size(x,2);
    err(6,ndx) = norm(f_temp1(t>=15&t<=25)-f(t(t>=15&t<=25)+2*dt+difft_r,5,((0.5-1)/(1+0.5)),1,15,1/(2*sqrt(log(2))))',1)./norm(f(t(t>=15&t<=25)+2*dt+difft_r,5,((0.5-1)/(1+0.5)),1,15,1/(2*sqrt(log(2))))',1);
    err(7,ndx) = norm(f_temp2(t>=20&t<=30)-f(t(t>=20&t<=30)+dt+10+difft_t,5,(1/(1+0.5)),0.5,25,0.5/(2*sqrt(log(2))))',1)./norm(f(t(t>=20&t<=30)+dt+10+difft_t,5,(1/(1+0.5)),0.5,25,0.5/(2*sqrt(log(2))))',1);
    results.(['t',num2str(ndx)]) = t;
    results.(['probe',num2str(ndx)]) = probe;
    figure(error_fig_log_r)
    hold on
    plot(log(err(3,ndx)),log(dx*err(6,ndx)),'r.','MarkerSize',12)
    title('Error - reflected')
    xlabel('log nx')
    ylabel('log error')
    figure(error_fig_log_t)
    hold on
    plot(log(err(3,ndx)),log(dx*err(7,ndx)),'r.','MarkerSize',12)
    title('Error - transmitted')
    xlabel('log nx')
    ylabel('log error')
    figure(error_fig)
    subplot(2,1,1)
    hold on
    plot(err(3,ndx),dx*err(6,ndx),'r.')
    title('Error - reflected')
    xlabel('nx')
    ylabel('error')
    subplot(2,1,2)
    hold on
    plot(err(3,ndx),dx*err(7,ndx),'r.')
    title('Error - transmitted')
    h=figure;
    figure(h)
    plot(t(t>=15&t<=25),f_temp1(t>=15&t<=25),'b',t(t>=15&t<=25),f(t(t>=15&t<=25)+2*dt+difft_r,5,((0.5-1)/(1+0.5)),1,15,1/(2*sqrt(log(2))))','k--')
    title('Comparisson, reflected')
    ylabel('f / calculated (a.u.)')
    xlabel('time (s)')
    legend('calculated','f - exact')
    print(h,'-depsc2',['fig_r_',num2str(ndx),'.eps'])
    close(h)
    clear h
    h=figure;
    figure(h)
    plot(t(t>=20&t<=30),f_temp2(t>=20&t<=30),'b',t(t>=20&t<=30),f(t(t>=20&t<=30)+dt+10+difft_t,5,(1/(1+0.5)),0.5,25,0.5/(2*sqrt(log(2))))','k--')
    title('Comparisson, transmitted')
    ylabel('f / calculated (a.u.)')
    xlabel('time (s)')
    legend('calculated','f - exact')
    print(h,'-depsc2',['fig_t_',num2str(ndx),'.eps'])
    close(h)
    clear h
end
print(error_fig_log_r,'-depsc2','error_log_r.eps')
print(error_fig_log_t,'-depsc2','error_log_t.eps')
print(error_fig,'-depsc2','error_simple.eps')