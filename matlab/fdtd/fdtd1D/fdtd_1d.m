% This is a simple FDTD test in 1D

% Set up
clear all
close all

%% general setup
movie = 0; % [0,1] logical switch
convergence_test=0; % [0,1] logical switch
probe = [5 10 15];

%% Grid

% xi, xf define the grid + ABC cells
xi  = -5.0;
xf  = 25.0;

% Physical boundaries of the problem
xil =  0;
xir = 20;

% grid sizes, dx
dxs = 0.05;%[1e-3 5e-3 0.01 0.025 0.05 0.1 0.25 0.5 1]; % array - dx grid size

% final time
ti =  0;
tf = 35;
%% Material

epso = 1.0;%8.854187817e-12;
eps1 = 1.0;
eps2 = 4.0;
muo  = 1.0;%4e-7*pi;
mu1  = 1.0;
mu2  = 1.0;

% speed of wave at right and left boundaries
cil = 1.0;
cir = 0.5;

% indicate where material changes (interface)
xc   = 10.0;

%% Scattering boundary conditions

A    = 1.0; % wave amplitude
s    = 1/(2.0*sqrt(2.0*log(2.0))); % sigma
to   = 5.0;

% define intial pulse shape (q,f are given for postporcessing)
g = @(t,x,c,to,s) exp(-((t-to-x./c).^2)./(2*(s.^2)));

q = @(t,x,c,to,s) exp(-((x-c.*(t-to)).^2)./(s.^2));
f = @(t,x,amp,c,to,s) amp.*exp(-((x-c.*(t-to)).^2)./(s.^2));


%% Pre calculations

co   = 1/sqrt(epso*muo);
zo   = sqrt(muo/epso);
cmin = co/(max(sqrt(eps1*mu1),sqrt(eps2*mu2)));

if convergence_test==1
    error_fig_log = figure;
    error_fig = figure;
end

num_dx = size(dxs,2);

for ndx = 1:num_dx
    
    % patch
    dx = dxs(ndx);
    x  = xi:dx:xf;
    nx = size(x,2);
    dt = 0.9/(co*sqrt(1/(dx^2)));
    nt = round(tf/dt);
    
    % pre-allocate probe
    probe(2:nt+2,:) = 0.0;
    
    % pre-calculations
    dtdx = dt/dx;
    
    % eps
    eps = (eps1*(x<10.0)) + (eps2*(x>10.0)) + (((eps1+eps2)/2.0)*(x==10.0));
    eps = epso*eps;
    
    % mu
    mu = (mu1*(x<10.0)) + (mu2*(x>10.0)) + (((mu1+mu2)/2.0)*(x==10.0));
    mu = muo*mu;
    
    % refractive index
    ni = sqrt(eps.*mu);
    
    % wave speed
    c = sqrt(eps.*mu);
    
    % Initial conditions for the fields
    Ey = zeros(nt+1,nx);
    Hz = zeros(nt,nx);
    
    % boundaries
    [~,il] = min(abs(x - xil));
    [~,ir] = min(abs(x - xir));
    
    ntf = round(nt/100);
   
    %% main FDTD loop
    for n=2:nt
        if mod(n,ntf)==0
            pct = round(n*100/nt);
            fprintf('%3f \n',pct)
        end

        % Update H
        for i=1:nx-1
            dm = dtdx/mu(i);
            Hz(n,i) = Hz(n-1,i) - dm*(Ey(n,i+1) - Ey(n,i));
            if i==il
                Hz(n,i) = Hz(n,i) + dm*g(n*dt,xil,cil,to,s);
            elseif i==ir
                Hz(n,i) = Hz(n,i) - dm*g(n*dt,xir,cir,to,s);
            end
        end
        
        % Update E
        for i=1:nx
            if i==1
                Ey(n+1,i) = Ey(n-1,i+1);
            elseif i==nx
                Ey(n+1,i) = Ey(n-1,i-1);
            else
                de = dtdx/eps(i);
                Ey(n+1,i) = Ey(n,i) - de*(Hz(n,i) - Hz(n,i-1));
            end
            
            if i==il
                Ey(n+1,i) = Ey(n+1,i) + de*zo*g((n+0.5)*dt,xil-dx/2,cil,to,s);
            elseif i ==ir
                Ey(n+1,i) = Ey(n+1,i) - de*zo*g((n+0.5)*dt,xir+dx/2,cir,to,s);
            end
        end
    end
    
    %% probe
    for j=1:size(probe,2)
        [c,index] = min(abs(x - probe(1,j,1)));
        probe(2:nt+2,j) = Ey(:,index);
    end
    
    %% make movie
    if movie==1
        for n=2:nt
            subplot(size(probe,2)+1,1,1)
            plot(x,Ey(n,:));axis([xi xf -1 2]);
            hold on
            plot(x,ni,'k--');axis([xi xf -1 2]);
            title(['t=',num2str(n*dt)])
            hold off
            for k = 2:size(probe,2)+1
                subplot(size(probe,2)+1,1,k)
                hold on
                plot(n*dt,probe(n,k-1));axis([0 tf -1 1])
                title(['probe at x=',num2str(probe(1,k-1))])
                hold off
            end
            M(:,n-1) = getframe;
        end
        results.(['movie',num2str(ndx)]) = M;
    end
    results.(['probe',num2str(ndx)]) = probe;
    
    %% Convergence test (error)
    if convergence_test==1
        t=0:tf/nt:tf;
        f_temp1 = probe(2:end,1);
        f_temp2 = probe(2:end,3);
        [ct,indt] = max(f_temp2(t>=20&t<=30));
        [ct2,indt2] = max(f(t(t>=20&t<=30)+dt+10,5,(1/(1+0.5)),0.5,25,0.5/(2*sqrt(log(2)))));
        difft_t = t(indt2)-t(indt);
        [cr,indr] = min(f_temp1(t>=15&t<=25));
        [cr2,indr2] = min(f(t(t>=15&t<=25)+2*dt,5,((0.5-1)/(1+0.5)),1,15,1/(2*sqrt(log(2)))));
        difft_r = t(indr2)-t(indr);
        err(1,ndx) = ndx;
        err(2,ndx) = dx;
        err(3,ndx) = size(x,2);
        err(4,ndx) = norm(f_temp1(t>=15&t<=25)-f(t(t>=15&t<=25)+2*dt+difft_r,5,((0.5-1)/(1+0.5)),1,15,1/(2*sqrt(log(2))))',1);
        err(5,ndx) = norm(f_temp2(t>=20&t<=30)-f(t(t>=20&t<=30)+dt+10+difft_t,5,(1/(1+0.5)),0.5,25,0.5/(2*sqrt(log(2))))',1);
        results.(['t',num2str(ndx)]) = t;
        figure(error_fig_log)
        subplot(2,1,1)
        hold on
        plot(log(size(x,2)),dx*log(err(4,ndx)),'r.')
        title('Error - reflected')
        xlabel('log nx')
        ylabel('log error')
        subplot(2,1,2)
        hold on
        plot(log(size(x,2)),dx*log(err(5,ndx)),'r.')
        title('Error - transmitted')
        figure(error_fig)
        subplot(2,1,1)
        hold on
        plot(size(x,2),dx*err(4,ndx),'r.')
        title('Error - reflected')
        xlabel('nx')
        ylabel('error')
        subplot(2,1,2)
        hold on
        plot(size(x,2),dx*err(5,ndx),'r.')
        title('Error - transmitted')
        h=figure;
        figure(h)
        plot(t(t>=15&t<=25),f_temp1(t>=15&t<=25),'b',t(t>=15&t<=25),f(t(t>=15&t<=25)+2*dt+difft_r,5,((0.5-1)/(1+0.5)),1,15,1/(2*sqrt(log(2))))','k--')
        title('Comparisson, reflected')
        ylabel('f / calculated (a.u.)')
        xlabel('time (s)')
        legend('calculated','f - exact')
        print(h,'-dpng',['fig_r_',num2str(ndx),'.png'])
        close(h)
        clear h
        h=figure;
        figure(h)
        plot(t(t>=20&t<=30),f_temp2(t>=20&t<=30),'b',t(t>=20&t<=30),f(t(t>=20&t<=30)+dt+10+difft_t,5,(1/(1+0.5)),0.5,25,0.5/(2*sqrt(log(2))))','k--')
        title('Comparisson, transmitted')
        ylabel('f / calculated (a.u.)')
        xlabel('time (s)')
        legend('calculated','f - exact')
        print(h,'-dpng',['fig_t_',num2str(ndx),'.png'])
    end
    clear probe f_temp1 f_temp2 Ey Hz
end

%% IO save files
if convergence_test==1
    print(error_fig,'-dpng','erra.png')
    print(error_fig_log,'-dpng','erra_log.png')
    results.err = err;
end

save('resultsa.mat','results','f','g','q')
