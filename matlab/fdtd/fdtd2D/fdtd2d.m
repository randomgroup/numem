close all
clear all
num_frames = 10;
save_outdir = './results';
before_step = 1;
debug=1;
iplot=0;
source=1;
initial_q=0;

%% Simulation parameters
% -----------------------------------------------------
% Simulation space
params.x_lower = 0.0;
params.x_upper = 1.0;
params.y_lower = 0.0;
params.y_upper = 1.0;

params.nx = 256;
params.ny = 256;

params.pml_size = 32;

solver.cfl_desired = 0.9;



% TFSF parameters (global position considerin pml region)
tfsf.nib = params.pml_size;
tfsf.njl = params.pml_size;
tfsf.njr = params.nx + params.pml_size; %+ 1;
tfsf.nit = params.ny + params.pml_size; %+ 2; 


% Definition of constants
eo = 8.854187817e-12;         % vacuum permittivity   - [F/m]
mo = 4e-7*pi;                 % vacuum peremeability  - [V.s/A.m]
co = 1.0/sqrt(eo*mo);         % vacuum speed of light - [m/s]
zo = sqrt(mo/eo);             % vaccuum impedance


%% Calculation and FDTD
% -----------------------------------------------------

% Create grid object
grid.x_upper = params.x_upper;
grid.y_upper = params.y_upper;
grid.x_lower = params.x_lower;
grid.y_lower = params.y_lower;
grid.nx = params.nx;
grid.ny = params.ny;

grid.x = linspace(params.x_lower,params.x_upper,params.nx);
grid.y = linspace(params.y_lower,params.y_upper,params.ny);

grid.delta = [grid.x(2)-grid.x(1),grid.y(2)-grid.y(1)];
grid.dx = grid.delta(1);
grid.dy = grid.delta(2);

grid.ny_ghost = 2*params.pml_size + grid.ny;
grid.nx_ghost = 2*params.pml_size + grid.nx;
grid.x_ghost = linspace(params.x_lower - params.pml_size*grid.dx,params.x_upper + params.pml_size*grid.dx,grid.nx+2*params.pml_size);
grid.y_ghost = linspace(params.y_lower - params.pml_size*grid.dy,params.y_upper + params.pml_size*grid.dx,grid.ny+2*params.pml_size);

[grid.X,grid.Y] = meshgrid(grid.x_ghost,grid.y_ghost);
R = sqrt((grid.X - (params.nx - 1)*grid.dx/2).^2 + (grid.Y - (params.ny - 1)*grid.dy/2).^2);


%% Incident wave generation
tfsf.num_points = grid.nx + params.pml_size;
tfsf.nrel = floor(params.pml_size/2);

tfsf.q3 = zeros(1,tfsf.num_points);
tfsf.q2 = zeros(1,tfsf.num_points);
tfsf.q2bc1 = 0.0;
tfsf.q2bc2 = 0.0;

yb = 0;%tfsf.nib*grid.dy;
yt = 1;%tfsf.nit*grid.dy;
xl = 0;%tfsf.njl*grid.dx;
xr = 1;%tfsf.njr*grid.dx;

%% Material profile
% Vacuum
vac(1) = mo;
vac(2) = mo;
vac(3) = eo;

eta1r = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);
eta2r = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);
eta3r = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);

eta1 = vac(1)*eta1r;
eta2 = vac(2)*eta2r;
eta3 = vac(3)*eta3r;

%% Precalculation
dx = grid.delta(1);%(grid.x_upper - grid.x_lower)/grid.nx_ghost;
dy = grid.delta(2);%(grid.x_upper - grid.x_lower)/grid.ny_ghost;
dt = solver.cfl_desired/(co*sqrt(1/dx^2 + 1/dy^2));
tfinal = (grid.x_upper - grid.x_lower)/co;
num_out = floor(tfinal/dt)+150;
cmax = 1.3^(1/grid.dx);
sigmabase = 1e-2;

%% Build PML
% pre-allocate
eta_dx_q1   = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);
eta_dy_q2   = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);
eta_dx_q2   = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);
eta_dy_s1   = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);
eta_dx_s3   = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);
eta_dy_q3   = ones(grid.ny_ghost + 1,grid.nx_ghost + 1);

sigma_dx_q1 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
sigma_dy_q2 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
sigma_dx_q2 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
sigma_dy_q3 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
sigma_dy_s1 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1); 
sigma_dx_s3 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);


for i = 1 : grid.ny_ghost + 1
    for j = 1 : grid.nx_ghost
        if j <= params.pml_size
            x = (params.pml_size - j)*dx + dx/2;
            sigma_dx_q1(i,j) = (cmax^x)*sigmabase;
            eta_dx_q1(i,j) = cmax^x;
        elseif j >= grid.nx_ghost - params.pml_size
            x = (j - params.pml_size - grid.ny - 1)*dx + dx/2;
            sigma_dx_q1(i,j) = (cmax^x)*sigmabase;
            eta_dx_q1(i,j) = cmax^x;
        end
    end
end

%Calculate eta_dy_s1 and sigma_dy_s1
for i = 1 : grid.ny_ghost + 1
    for j = 1 : grid.nx_ghost
        if i <= params.pml_size + 1
            y = (params.pml_size + 1 - i)*dy;
            sigma_dy_s1(i,j) = (cmax^y)*sigmabase;
            eta_dy_s1(i,j) = cmax^y;
        elseif i >= params.pml_size + grid.nx + 1
            y = (i - params.pml_size - grid.nx - 1)*dy;
            sigma_dy_s1(i,j) = (cmax^y)*sigmabase;
            eta_dy_s1(i,j) = cmax^y;
        end
    end
end

%Calculate eta_dy_q2 and sigma_dy_q2
for i = 1 : grid.ny_ghost
    for j = 1 : grid.nx_ghost + 1
        if i <= params.pml_size
            y = (params.pml_size - i)*dy + dy/2; sigma_dy_q2(i,j) = (cmax^y)*sigmabase;
            eta_dy_q2(i,j) = cmax^y;
        elseif i >= params.pml_size + grid.nx + 1
            y = (i - params.pml_size - grid.nx - 1)*dy + dy/2;
            sigma_dy_q2(i,j) = (cmax^y)*sigmabase;
            eta_dy_q2(i,j) = cmax^y;
        end
    end
end

%Calculate eta_dx_q2 and sigma_dx_q2
for i = 1 : grid.ny_ghost
    for j = 1 : grid.nx_ghost + 1
        if j <= params.pml_size + 1
            x = (params.pml_size + 1 - j)*dx;
            sigma_dx_q2(i,j) = (cmax^x)*sigmabase;
            eta_dx_q2(i,j) = cmax^x;
        elseif j >= params.pml_size + grid.ny + 1
            x = (j - params.pml_size - grid.ny - 1) * dx;
            sigma_dx_q2(i,j) = (cmax^x)*sigmabase;
            eta_dx_q2(i,j) = cmax^x;
        end
    end
end

%Calculate the eta_dx_s3 and sigma_dx_s3
for i = 1 : grid.ny_ghost
    for j = 1 : grid.nx_ghost
        if j <= params.pml_size
            x = (params.pml_size - j)*dx + dx/2;
            sigma_dx_s3(i,j) = (cmax^x)*sigmabase;
            eta_dx_s3(i,j) = cmax^x;
        elseif j >= grid.nx_ghost - params.pml_size
            x = (j - params.pml_size - grid.ny - 1)*dx + dx/2;
            sigma_dx_s3(i,j) = (cmax^x)*sigmabase;
            eta_dx_s3(i,j) = cmax^x;
        end
    end
end

%Calculate eta_dy_q3 and sigma_dy_q3
for i = 1 : grid.ny_ghost
    for j = 1 : grid.nx_ghost
        if i <= params.pml_size
            y = (params.pml_size - i)*dy + dy/2;
            sigma_dy_q3(i,j) = (cmax^y)*sigmabase;
            eta_dy_q3(i,j) = cmax^y;
        elseif i >= params.pml_size + grid.nx + 1
            y = (i - params.pml_size - grid.nx - 1)*dy + dy/2;
            sigma_dy_q3(i,j) = (cmax^y)*sigmabase;
            eta_dy_q3(i,j) = cmax^y;
        end
    end
end


nt_q3 = 1;
nt_q2 = 1;
q1_old = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
q2_old = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
q3_old = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
s1_old = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
s2_old = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
s3_old = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);

q1 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
q2 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
q3 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
s1 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
s2 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);
s3 = zeros(grid.ny_ghost + 1,grid.nx_ghost + 1);

q1_save = zeros(grid.ny_ghost,grid.nx_ghost,num_out - 1);
q2_save = zeros(grid.ny_ghost,grid.nx_ghost,num_out - 1);
q3_save = zeros(grid.ny_ghost,grid.nx_ghost,num_out - 1);

pml = zeros(15,grid.ny_ghost + 1,grid.nx_ghost + 1);
eoo = 2.0*eo;
D1 = dt/dx;
D2 = dt/dy;

if debug==1
    fig_debug = figure;
end

if iplot==1
    fig_iplot = figure;
end

for i=1:grid.ny_ghost+1
    for j=1:grid.nx_ghost
        temp_gamma1 = eoo*eta_dx_q1(i,j) - sigma_dx_q1(i,j)*dt;
        temp_gamma2 = eoo*eta_dy_s1(i,j) - sigma_dy_s1(i,j)*dt;

        temp_zeta1 = eoo*eta_dx_q1(i,j) + sigma_dx_q1(i,j)*dt;
        temp_zeta2 = eoo*eta_dy_s1(i,j) + sigma_dy_s1(i,j)*dt;
        temp_zeta3 = eoo;

        pml(1,i,j) = eoo*D2/temp_zeta2;
        pml(2,i,j) = temp_gamma2/temp_zeta2;
        pml(3,i,j) =  temp_zeta1/(eta1(i,j)*temp_zeta3);
        pml(4,i,j) = temp_gamma1/(eta1(i,j)*temp_zeta3);
    end
end

for i = 1 : grid.ny_ghost
    for j = 1 : grid.nx_ghost + 1
        temp_gamma1 = eoo*eta_dx_q2(i,j) - sigma_dx_q2(i,j)*dt;
        temp_gamma2 = eoo*eta_dy_q2(i,j) - sigma_dy_q2(i,j)*dt;

        temp_zeta1  = eoo*eta_dx_q2(i,j) + sigma_dx_q2(i,j)*dt;
        temp_zeta2  = eoo*eta_dy_q2(i,j) + sigma_dy_q2(i,j)*dt;

        pml(5,i,j) = D1;
        pml(6,i,j) =  temp_zeta2/(eta2(i,j)*temp_zeta1);
        pml(7,i,j) = temp_gamma2/(eta2(i,j)*temp_zeta1);
        pml(8,i,j) = temp_gamma1/temp_zeta1;
    end
end

for i = 1:grid.ny_ghost
    for j = 1:grid.nx_ghost
        temp_gamma1 = eoo*eta_dx_s3(i,j) - sigma_dx_s3(i,j)*dt;
        temp_gamma2 = eoo*eta_dy_q3(i,j) - sigma_dy_q3(i,j)*dt;

        temp_zeta1  = eoo*eta_dx_s3(i,j) + sigma_dx_s3(i,j)*dt;
        temp_zeta2  = eoo*eta_dy_q3(i,j) + sigma_dy_q3(i,j)*dt;

        pml( 9,i,j) = eoo*dt/temp_zeta1;
        pml(10,i,j) = temp_gamma1/temp_zeta1;
        pml(11,i,j) = temp_gamma2/temp_zeta2;
        pml(12,i,j) =  eoo/(eta3(i,j)*temp_zeta2);
        pml(13,i,j) =  eoo/(eta3(i,j)*temp_zeta2);
    end
end

%Excitation of q3
nt = 0.5;
tfsf.delay = floor(tfsf.nrel*grid.dx/co/dt)*dt;%60*dt;
tfsf.sigma = 10*dt;
if source==1
    tfsf.q3(1,1) = exp(-(((nt*dt - tfsf.delay)/(tfsf.sigma))^2));
else
    tfsf.q3(1,1) = 0;
end



if initial_q==1
    q3_old(params.pml_size+1:end-params.pml_size-1,params.pml_size+1:end-params.pml_size-1) = ...
        zo.*exp(-(R(params.pml_size:end-params.pml_size-1,params.pml_size:end-params.pml_size-1).^2)./0.04);
    q2_old(params.pml_size+1:end-params.pml_size-1,params.pml_size+1:end-params.pml_size-1) = ...
        exp(-(R(params.pml_size:end-params.pml_size-1,params.pml_size:end-params.pml_size-1).^2)./0.04);
    pcolor(q3_old)
end



while nt<num_out
    nt = nt + 0.5;
    nt_q2 = nt_q2 + 1;
    
    % Calculate s1/q1
    for i = 1:grid.ny_ghost + 1
        for j = 1:grid.nx_ghost  
            
            dq3incb = 0;
            dq3inct = 0;
            
            if(i==tfsf.nib && j>=tfsf.njl && j<=tfsf.njr)
                k = tfsf.nrel + j - params.pml_size;
                dq3incb = -pml(1,i,j)*tfsf.q3(1,k);
            elseif(i==tfsf.nit && j>=tfsf.njl && j<=tfsf.njr)
                k = tfsf.nrel + j - params.pml_size;
                dq3inct = pml(1,i,j)*tfsf.q3(1,k);
            end

            if i==1
                dq3 =  q3_old(i,j);
            elseif i==(grid.ny_ghost + 1)
                dq3 = -q3_old(i-1,j);
            else
                dq3 =  q3_old(i,j) - q3_old(i-1,j);
            end
            
            s1(i,j) = pml(2,i,j)*s1_old(i,j) + pml(1,i,j)*dq3 + dq3incb + dq3inct;
            q1(i,j) = q1_old(i,j) + pml(3,i,j)*s1(i,j) - pml(4,i,j)*s1_old(i,j);
        end
    end
    
    %Calculate s2/q2
    for i = 1 : grid.ny_ghost
        for j = 1 : grid.nx_ghost + 1

            dq3incl = 0;
            dq3incr = 0;
            
            if(j==tfsf.njl && i>=tfsf.nib && i<=tfsf.nit)
                k = tfsf.nrel;
                dq3incl= -pml(5,i,j)*tfsf.q3(1,k);
            elseif(j==tfsf.njr && i>=tfsf.nib && i<=tfsf.nit)
                k = grid.nx + tfsf.nrel;
                dq3incr = pml(5,i,j)*tfsf.q3(1,k);
            end

            if j == 1
                dq3 =  q3_old(i,j);
            elseif j == grid.nx_ghost + 1
                dq3 = -q3_old(i,j - 1);
            else
                dq3 =  q3_old(i,j) - q3_old(i,j-1);
            end
            
            s2(i,j) = s2_old(i,j) + pml(5,i,j)*dq3 + dq3incl + dq3incr;     
            q2(i,j) = pml(6,i,j)*s2(i,j) - pml(7,i,j)*s2_old(i,j) + pml(8,i,j)*q2_old(i,j);
        end
    end
    
    % Update tfsf.q2
    for i = 2:tfsf.num_points
        tfsf.q2(1,i) = tfsf.q2(1,i) - (D1/eta2(floor(grid.ny_ghost/2),i+tfsf.nrel))*(tfsf.q3(1,i) - tfsf.q3(1,i-1));
    end
    
    % Absorb the 1D auxiliary field... how? ABCs require the solution at times n-1 and n

   if(mod(nt_q2,2))
        tfsf.q2(1,tfsf.num_points) = tfsf.q2bc1;
        tfsf.q2bc1 =  tfsf.q2(1,tfsf.num_points - 1);
    else
        tfsf.q2(1,tfsf.num_points) = tfsf.q2bc2;
        tfsf.q2bc2 =  tfsf.q2(1,tfsf.num_points - 1);
   end
    
    nt = nt + 0.5;
    nt_q3 = nt_q3 + 1;
    
    %Calculate s3/q3
    for i = 1:grid.ny_ghost
        for j = 1:grid.nx_ghost
            dq2 = q2(i,j+1) - q2(i,j);
            dq1 = q1(i+1,j) - q1(i,j);

            dq2incl = 0;
            dq2incr = 0;
            if(j==tfsf.njl && i>=tfsf.nib && i<=tfsf.nit)
                k = tfsf.nrel;
                dq2incl = (pml(9,i,j)/dx)*tfsf.q2(1,k);
            elseif(j==tfsf.njr && i>=tfsf.nib && i<=tfsf.nit)
                k = grid.nx + tfsf.nrel;
                dq2incr = -(pml(9,i,j)/dx)*tfsf.q2(1,k);
            end
            
            s3(i,j) = (pml(9,i,j)/dy)*dq2 + (pml(9,i,j)/dx)*dq1 + pml(10,i,j)*s3_old(i,j) + dq2incl + dq2incr;
            q3(i,j) = pml(11,i,j)*q3_old(i,j) + pml(12,i,j)*s3(i,j) - pml(13,i,j)*s3_old(i,j);
        end
    end
    % Update tfsf.q3
    for i = 2:tfsf.num_points-1
        tfsf.q3(1,i) = tfsf.q3(1,i) - (D1/eta3(floor(grid.ny_ghost/2),i+tfsf.nrel))*(tfsf.q2(1,i+1) - tfsf.q2(1,i));
    end
    
    % set boundary condtfsf.nitions inside
    if before_step==1
        for i = 1:grid.ny_ghost
            for j=1:grid.nx_ghost
                xtry = grid.x_ghost(1,j);
                ytry = grid.y_ghost(1,i);
                rtemp = sqrt((xtry-(grid.x_upper-grid.x_lower)/2)^2 + ...
                    (ytry-(grid.y_upper-grid.y_lower)/2)^2);
                if rtemp<0.2
                    if (ytry-(grid.y_upper-grid.y_lower)/2)<0    
                        %q1(i+1,j) = 0.0;
                        %q2(i+1,j) = 0.0;
                        q3(i+1,j) = 0.0;
                    else
                        %q1(i,j) = 0.0;
                        %q2(i,j) = 0.0;
                        q3(i,j) = 0.0;
                    end
                end
            end
        end
    end

    
    if source==1
        %tfsf.q3(1,1) = %sin(2*pi*5e9*nt*dt);
        tfsf.q3(1,1) = exp(-(((nt*dt - tfsf.delay)/(tfsf.sigma))^2));
    else
        tfsf.q3(1,1) = 0.0;
    end
    q3_old = q3;
    s1_old = s1;
    q1_old = q1;
    s2_old = s2;
    q2_old = q2;
    s3_old = s3;
    
    % Saving and plotting perafernalia
%     q1_save(1:grid.ny_ghost,1:grid.nx_ghost,nt_q3) = q1(1:grid.ny_ghost,1:grid.nx_ghost);
%     q2_save(1:grid.ny_ghost,1:grid.nx_ghost,nt_q3) = q2(1:grid.ny_ghost,1:grid.nx_ghost);
%     q3_save(1:grid.ny_ghost,1:grid.nx_ghost,nt_q3) = q3(1:grid.ny_ghost,1:grid.nx_ghost);
    if debug==1
        figure(fig_debug)
        subplot(2,1,1)
        pcolor(grid.X,grid.Y,q3(1:grid.ny_ghost,1:grid.nx_ghost)), shading interp
        set(gca,'DataAspectRatio',[1 1 1],...
            'PlotBoxAspectRatio',[1 1 1],...
            'ZLim',[-2.0 2.0])
        colorbar
        hold on
        plot(xl.*ones(1,grid.nx),grid.y_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),'k--')
        plot(xr.*ones(1,grid.nx),grid.y_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),'k--')
        plot(grid.x_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),yt.*ones(1,grid.ny),'k--')
        plot(grid.x_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),yb.*ones(1,grid.ny),'k--')
        caxis([-1 1])
        pause(1e-15)
        hold off
        subplot(2,1,2)
        plot(tfsf.q3(1,:))
    end
    if iplot==1
        figure(fig_iplot)
        pcolor(grid.X,grid.Y,q3_save(:,:,nt_q3)), shading interp
        set(gca,'DataAspectRatio',[1 1 1],...
            'PlotBoxAspectRatio',[1 1 1],...
            'ZLim',[-2.0 2.0])
        colorbar
        hold on
        plot(xl.*ones(1,grid.nx),grid.y_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),'k--')
        plot(xr.*ones(1,grid.nx),grid.y_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),'k--')
        plot(grid.x_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),yt*ones(1,grid.ny),'k--')
        plot(grid.x_ghost(params.pml_size+1:grid.ny_ghost-params.pml_size),yb*ones(1,grid.ny),'k--')
        caxis([-1 1])
        pause(1e-15)
        hold off
    end
end