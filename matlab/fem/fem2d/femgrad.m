function [phi_x,phi_y] = femgrad(phi,mesh)
phi_x = zeros(size(mesh.t,1),1);
phi_y = phi_x;
for e = 1:size(mesh.t,1)
    % get local points
    re = mesh.p(mesh.t(e,:),:);
    
    % calculate inverse coefficients
    %a(1) = re(2,1)*re(3,2) - re(3,1)*re(2,2);
    %a(2) = re(3,1)*re(1,2) - re(1,1)*re(3,2);
    %a(3) = re(1,1)*re(2,2) - re(2,1)*re(1,2);
    phie = phi(mesh.t(e,:));
    
    b(1) = re(2,2) - re(3,2);
    b(2) = re(3,2) - re(1,2);
    b(3) = re(1,2) - re(2,2);
    
    c(1) = -re(2,1) + re(3,1);
    c(2) = -re(3,1) + re(1,1);
    c(3) = -re(1,1) + re(2,1);
    
    % calculate  area of element
    deltae = (b(1)*c(2) - b(2)*c(1));
    
    
    phi_x(e,1) = b*phie/deltae;
    phi_y(e,1) = c*phie/deltae;
    
end
end