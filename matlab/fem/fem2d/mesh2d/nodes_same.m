function [same] = nodes_same(N1,N2)
same = 0;
if abs(N1(1)-N2(1))<1.0e-5;
    % X coordinate is the same
    if abs(N1(2)-N2(2))<1.0e-5;
        % Y corrdinate is the same
        same = 1;
        return
    end
end