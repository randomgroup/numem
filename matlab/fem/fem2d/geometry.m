function [p,t,b] = geometry(params)

% Geometry Coaxial cable
% Input parameters
rout = params.rout; % outer radius of the domain
rin = params.rin; % inner radius of the domain
center_shift = params.center_shift; % the center of the inner circle
mesh_size=params.mesh_size; % volumetric mesh size parameter
nof_edg_out=params.nof_edg_out; % number of edges on the outer circle
nof_edg_in=params.nof_edg_in; % number of edges on the inner circle

if size(center_shift,2)==2
    center_shift = repmat(center_shift,nof_edg_in,1);
end
hdata = [];
hdata.hmax = mesh_size;   

% ============== outer boundary =====================
dtheta = 2*pi/nof_edg_out;
theta  = (-pi:dtheta:(pi-dtheta))';
% polygon for the outer circle
node1   = rout*[cos(theta) sin(theta)];
cnect1=zeros(nof_edg_out,2);
for i=1:length(node1)-1
    cnect1(i,1)= i;
    cnect1(i,2)= i+1;
end
i = length(node1);
cnect1(i,1)= i;
cnect1(i,2)= 1;

len=zeros(nof_edg_out,1);
% find the edge lengths to make sure everything is OK
for i=1:length(cnect1)
    n1 = node1(cnect1(i,1),:);
    n2 = node1(cnect1(i,2),:);
    diff = n2-n1;
    len(i)= sqrt(dot(diff,diff));
end
if (max(len)>mesh_size/4)
    display('increase the number of edges on the outer boundary');
    display('you may get unexpected results');
   pause
end

% ============== inner boundary =====================
dtheta = 2*pi/nof_edg_in;
theta  = (0:dtheta:(2*pi-dtheta))';
node2   = rin*[cos(theta) sin(theta)]-center_shift;
shift = length(node1);
for i=1:length(node2)-1
    cnect2(i,1)= i+shift;
    cnect2(i,2)= i+shift+1;
end
i = length(node2);
cnect2(i,1)= i+shift;
cnect2(i,2)= 1+shift;

len=zeros(nof_edg_in,1);
% find the edge lengths to make sure everything is OK
for i=1:length(cnect2)
    n1 = node2(cnect2(i,1)-shift,:);
    n2 = node2(cnect2(i,2)-shift,:);
    diff = n2-n1;
    len(i)= sqrt(dot(diff,diff));
end
if (max(len)>mesh_size/4)
    display('increase the number of edges on the inner boundary');
    display('you may get unexpected results');
   pause
end


cnect_boundary = [cnect1; cnect2];
bp = [node1; node2];
% Make mesh
[p,t] = mesh2d(bp,cnect_boundary,hdata);


% mark the node indices
for i=1:length(p)
    text(p(i,1),p(i,2),num2str(i));
end

% mark the edges
for i=1:length(cnect_boundary)
    n1 = bp(cnect_boundary(i,1),:);
    n2 = bp(cnect_boundary(i,2),:);
    h=text(0.5*(n1(1)+n2(1)),0.5*(n1(2)+n2(2)),num2str(i));
    set(h,'color','r');
end


% update the cnect_boundary with new node numbers
% assumes there is one-to-one matching with initial
% nodes on the boundaries and boudary nodes generated
% by mesh2d
for i=1:length(cnect_boundary)
    % first node
    n1 = bp(cnect_boundary(i,1),:);
    for j = 1:length(p)
        j1 = p(j,:);
        % check the first node
        [same_node]=nodes_same(n1,j1);
        if (same_node==1)
            b(i,1)=j;
            break
        end
    end
    % second node
    n2 = bp(cnect_boundary(i,2),:);
    for j = 1:length(p)
        j1 = p(j,:);
        % check the first node
        [same_node]=nodes_same(n2,j1);
        if (same_node==1)
            b(i,2)=j;
            break
        end
    end
end
% cnect_boundary_updated stores the connectivity
% information for boundary nodes and edges (using
% the node indexing provided by the mesh2d (but not
% the node provided by the user)
end
