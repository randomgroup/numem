function [K,F] = femkp(mesh,params)
% function [K,F] = femk(mesh,fe)
%
% p is the volume coordinates of each point
% t is the list of nodes per element 
%
% 
% f can be an averaged functionis an averaged function along each element in the order of t, or the
% barycentric value of f along each element. 

% initialize global matrices 

K = zeros(size(mesh.p,1),size(mesh.p,1));
F = zeros(size(mesh.p,1),1);

for e = 1:size(mesh.t,1)
    % get local points
    re = mesh.p(mesh.t(e,:),:);
    
    % calculate inverse coefficients
    %a(1) = re(2,1)*re(3,2) - re(3,1)*re(2,2);
    %a(2) = re(3,1)*re(1,2) - re(1,1)*re(3,2);
    %a(3) = re(1,1)*re(2,2) - re(2,1)*re(1,2);
    
    b(1) = re(2,2) - re(3,2);
    b(2) = re(3,2) - re(1,2);
    b(3) = re(1,2) - re(2,2);
    
    c(1) = -re(2,1) + re(3,1);
    c(2) = -re(3,1) + re(1,1);
    c(3) = -re(1,1) + re(2,1);
    
    
    % calculate  area of element
    deltae = 0.5*(b(1)*c(2) - b(2)*c(1));
    
    % assemble coefficiets for element
    ce = [sqrt(params.alphae(e,1))*b; sqrt(params.alphae(e,2))*c];
    Ke = (ce(1:2,:)'*ce(1:2,:))/(4*deltae) + ...
         params.betae(e,1)*(ones(3,3) + eye(3))/12;
    fe(1:3,1) = params.f(e)*deltae/3;
    
    % assemble global matrix
    for i=1:3
        for j=1:3
            K(mesh.t(e,i),mesh.t(e,j)) = K(mesh.t(e,i),mesh.t(e,j)) + Ke(i,j);
        end
        F(mesh.t(e,i),1) = F(mesh.t(e,i),1) + fe(i,1);
    end
    
end
