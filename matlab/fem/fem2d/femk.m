function [K,F] = femk(p,t,f,ftype)
% function [K,F] = femk(p,t,fe)
%
% p is the volume coordinates of each point
% t is the list of nodes per element 
%
% 
% f can be an averaged functionis an averaged function along each element in the order of t, or the
% barycentric value of f along each element. 

% initialize global matrices 
if nargin<4
    ftype='nodal';
end
K = zeros(size(p,1),size(p,1));
F = zeros(size(p,1),1);

% fill each element
switch ftype
    case 'nodal'
    for element=1:size(t,1)
        A = [ones(3,1),p(t(element,:),:)];
        Ae  = abs(det(A));
        c = inv(A);
        Ke = Ae*(transpose(c(2:3,:)))*c(2:3,:);
        K(t(element,:),t(element,:)) = K(t(element,:),t(element,:)) + Ke;
        % calculate barycentric average
        fe = barycentric_average(p(t(element,:)',:),f(t(element,:)),...
                                orthocenter(p(t(element,:)',:)));
        F(t(element,:),1) = F(t(element,:),1) + fe*Ae/3;
    end
    
    case 'averaged' 
    for element=1:size(t,1)
        Ae  = abs(det([ones(3,1),p(t(element,:),:)]));
        c = inv([ones(3,1),p(t(element,:),:)]);
        Ke = Ae*(c(2:3,:)')*c(2:3,:);
        K(t(element,:),t(element,:)) = K(t(element,:),t(element,:)) + Ke;   
        F(t(element,:),1) = F(t(element,:),1) + f(element)*Ae/3;
    end
end