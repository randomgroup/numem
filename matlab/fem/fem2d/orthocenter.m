function p = orthocenter(vertices)

    %m1 = (vertices(2,2)-vertices(1,2))/(vertices(2,1)-vertices(1,1));
    m2 = (vertices(3,2)-vertices(2,2))/(vertices(3,1)-vertices(2,1));
    m3 = (vertices(3,2)-vertices(1,2))/(vertices(3,1)-vertices(1,1));
    
    m4 = -1/m2;
    m5 = -1/m3;
    %m6 = -1/m1;
    
    T = [-m4 1; -m5 1];
    b = [-m4*vertices(1,1)+vertices(1,2);-m5*vertices(2,1)+vertices(2,2)];
    p(1) = b(1)/(-m4 + m5) - b(2)/(-m4 + m5);
    p(2) = -((b(2)*m4)/(-m4 + m5)) + (b(1)*m5)/(-m4 + m5);
end
