function [K,F] = fembc_dirichlet(K,F,bce,bcp,mesh)


for e=bce(1):bce(2)
    nd = mesh.br(e);
    F(nd,1) = bcp(e);
    K(nd,nd) = 1;
    for j = 1:size(mesh.p,1)
        if j~=nd
            F(j) = F(j) - K(j,nd)*bcp(e);
            K(nd,j) = 0;
            K(j,nd) = 0;
        end
    end
end
    
end