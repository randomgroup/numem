function fe = barycentric_average(vertices,f,r)
    T = [vertices(1,1)-vertices(3,1) vertices(2,1)-vertices(3,1);...
         vertices(1,2)-vertices(3,2) vertices(2,2)-vertices(3,2)];
     
    rp = [r(1,1) - vertices(3,1); r(1,2) - vertices(3,2)];
    
    lambda = T\rp;
    
    lambda = lambda';
    lambda(3) = 1 - lambda(1) - lambda(2);
    
    fe = lambda(1)*f(1) + lambda(2)*f(2) + lambda(3)*f(3);
    
end
