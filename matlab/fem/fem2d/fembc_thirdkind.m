function [K,F] = fembc_thirdkind(K,F,bce,ubc,mesh)

for s=bce(1):bce(2)
    ns = mesh.b(s,:);
    i = mesh.b(s,1);
    j = mesh.b(s,2);
    
    ls = sqrt((mesh.p(i,1)-mesh.p(j,1))^2 +((mesh.p(i,2)-mesh.p(j,2))^2));
    
    Ks(1,1) = ubc.gamma(s)*ls/3;
    Ks(1,2) = ubc.gamma(s)*ls/6;
    Ks(2,1) = Ks(1,2);
    Ks(2,2) = Ks(1,1);
    
    fs = ubc.q(s)*ls/2;
    
    for m=1:2
        for k=i:2
            K(ns(m),ns(k)) = K(ns(m),ns(k)) + Ks(m,k);
        end
        
        F(ns(m),1) = F(ns(m),1) + fs;
    end
    
    
end

end
