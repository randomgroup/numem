close all
clear all

params.rout = 1; % outer radius of the domain
params.rin = 0.25; % inner radius of the domain
params.center_shift = 0; % the center of the inner circle
params.mesh_size=0.5; % volumetric mesh size parameter
params.nof_edg_out=100; % number of edges on the outer circle
params.nof_edg_in=25; % number of edges on the inner circle
params.save_mesh = 1;
params.save_name = 'teste';
params.print_results=1;
params.calc_err  = 1;
params.save_fig_err = 1;
params.calc_grad = 1;

% generate mesh
[mesh.p,mesh.t,mesh.b] = geometry(params);

% reorder b for Dirichlet boundary conditions
mesh.br = zeros(size(mesh.b,2)*size(mesh.b,1),1);
mesh.br(1:2:end,1) = mesh.b(:,1);
mesh.br(2:2:end,1) = mesh.b(:,2);

% get coordinates of nodes boundaries
mesh.tb = mesh.p(mesh.br,:);

% generate orthocentric coordinates
mesh.po = zeros(size(mesh.t,1),2);
for e=1:size(mesh.t,1)
    mesh.po(e,:) = orthocenter(mesh.p(mesh.t(e,:),:));
end


% Dirichlet boundary conditions
ubc.p  = zeros(size(mesh.br,1),1);
ubc.p(1:2*params.nof_edg_out,1) = 1.0;
ubc.p(2*params.nof_edg_out+1:end,1) = 0.0;

% fill material properties at every node
params.alpha(:,1) = ones(size(mesh.p,1),1);
params.alpha(:,2) = ones(size(mesh.p,1),1);
params.beta(:,1) = zeros(size(mesh.p,1),1);



% compute material properties at every element from node data
for e=1:size(mesh.t,1)
    params.alphae(e,1) = barycentric_average(mesh.p(mesh.t(e,:)',:),...
        params.alpha(mesh.t(e,:),1),...
        mesh.po(e,:));
    params.alphae(e,2) = barycentric_average(mesh.p(mesh.t(e,:)',:),...
        params.alpha(mesh.t(e,:),2),...
        mesh.po(e,:));
    
    params.betae(e,2) = barycentric_average(mesh.p(mesh.t(e,:)',:),...
        params.beta(mesh.t(e,:),1),...
        mesh.po(e,:));
end



if params.save_mesh
    print(gcf,'-dpsc2',[params.save_name,'_m',num2str(size(mesh.t,1)),'mesh.eps']);
end

% calculate f
params.f = zeros(size(mesh.t,1),1);

% matrix assemble
[K,F] = femkp(mesh,params);

% impose boundary conditions
[K,F] = fembc_dirichlet(K,F,[1 size(mesh.br,1)],ubc.p,mesh);

% create sparse matrix system
Ks = sparse(K);
Fs = sparse(F);

% solve the system of equations
phi = Ks\Fs;

% plot the results, field (gradient) and error

figure
trisurf(mesh.t,mesh.p(:,1),mesh.p(:,2),phi,'facecolor','interp');
axis equal
view(3)
if params.print_results
    print(gcf,'-dpsc2',[params.save_name,'_m',num2str(size(mesh.t,1)),'_v3.eps']);
    view(2)
    print(gcf,'-dpsc2',[params.save_name,'_m',num2str(size(mesh.t,1)),'_v2.eps']);
    view([90 0])
    print(gcf,'-dpsc2',[params.save_name,'_m',num2str(size(mesh.t,1)),'_v1.eps']);
    close
end
% calculate analytical solution for centered
%figure

if params.calc_grad
    [phi_x,phi_y]=femgrad(phi,mesh);
    figure
    quiver(mesh.po(:,1),mesh.po(:,2),-phi_x,-phi_y);
    print(gcf,'-dpsc2',[params.save_name,'_m',num2str(size(mesh.t,1)),'_grad.eps']);
    close
end