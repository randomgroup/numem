function solver = offdiagK(solver,grid)
    solver.Kij = zeros(1,grid.num_points-1);
    for i = 1:grid.num_points-1
        solver.Kij(1,i) = -solver.alpha(i)/grid.delta(i) + ...
                                    solver.beta(i)*(grid.delta(i)/6);
    end
end
