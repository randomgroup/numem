function solver = rightb(solver,grid)

    m = grid.num_points;
    h = grid.delta;

    solver.b = zeros(1,grid.num_points);
    
    solver.b(1,1) = solver.avg_f(1)*h(1)/2;
    solver.b(1,m) = solver.avg_f(m)*h(m)/2;
    for i=2:m-1
        solver.b(1,i) = solver.avg_f(i-1)*h(i-1)/2 + solver.avg_f(i)*h(i)/2;
    end
    solver.b = transpose(solver.b);
end