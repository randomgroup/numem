function solver = diagK(solver,grid)
    m = grid.num_points;
    h = grid.delta;
    solver.Kii = zeros(1,m);
    solver.Kii(1,1) = (solver.alpha(1)/h(1)) + solver.beta(1)*(h(1)/3);
    solver.Kii(1,m) = (solver.alpha(m)/h(m)) + solver.beta(m)*(h(m)/3);
    for i = 2:m-1
        solver.Kii(1,i) = (solver.alpha(i-1)/h(i-1)) + solver.beta(i-1)*(h(i-1)/3) + ...
                                (solver.alpha(i)/h(i)) + solver.beta(i)*(h(i)/3);
    end
    
end