clear all
close all
%% setup problem
% solver paramenter
solver.tolerance = 1e-4;
solver.max_iter = 40000;
solver.method = 'gauss_seidel';
solver.system = 'homogeneous';
solver.bc_type = 'Dirichlet';
solver.with_error_test = 1;
solver.debug = 0;
solver.natural = 0;
solver.f = @(x) -2.0.*(pi^2).*sin(pi.*x);
solver.y = @(x) exp(pi*x) - exp(-pi*x) + sin(pi*x);
solver.alpha0 = -1.0;
solver.beta0 = -pi^2;

controller.iplot = 1;
controller.calc_error = 1;

% set grid
x_lower = 0.0;
x_upper = 1.0;
% set boundary conditions
ubc.l = 0.0;
ubc.r = exp(pi) - exp(-pi);

if solver.natural
    figure1 = figure;
    axes1 = axes('Parent',figure1);
    hold(axes1,'all');
end

if controller.iplot
    figure2 = figure;
    axes2 = axes('Parent',figure2);
    hold(axes2,'all');
end

if controller.calc_error
    figure3 = figure;
    axes3 = axes('Parent',figure3);
    hold(axes3,'all');
end

tic
for test=2:10
    disp(test)
    grid.num_points = 2^(test);
    grid.x = linspace(x_lower,x_upper,grid.num_points);
    grid.delta = grid.x(2:end) - grid.x(1:end-1);
    grid.delta(1,grid.num_points) = grid.delta(end);
    
    solver.alpha = solver.alpha0.*ones(1,grid.num_points);
    solver.beta = solver.beta0*ones(1,grid.num_points);
    solver.avg_f = averagef(solver.f,grid,1);
    solver.avg_y = averagef(solver.y,grid,0);
    solver = diagK(solver,grid);
    solver = offdiagK(solver,grid);
    solver = rightb(solver,grid);
    solver.bp = solver.b(2:end-1,1);
    solver.bp(1) = solver.bp(1) - solver.Kij(1,1)*ubc.l;
    solver.bp(end) = solver.bp(end) - solver.Kij(1,end)*ubc.r;
    
    solver.Kiip = solver.Kii(1,2:end-1);
    solver.Kijp = solver.Kij(1,2:end-1);

    solver.Ap = diag(solver.Kiip) +  diag(solver.Kijp,-1) + diag(solver.Kijp,1);
    
    [B,d] = spdiags(solver.Ap);
    solver.Ap = spdiags(B,d,solver.Ap);
    
    
    solver.(['phip',num2str(test)]) = [ubc.l; solver.Ap\solver.bp; ubc.r];

    if controller.iplot
        plot(grid.x,solver.(['phip',num2str(test)]),'Parent',axes2,...
                'Marker','.','LineStyle','--','DisplayName',num2str(2^test))
    end
    
    if solver.natural
        solver.A = diag(solver.Kii) +  diag(solver.Kij,-1) + diag(solver.Kij,1);
        [B,d] = spdiags(solver.A);
        solver.A = spdiags(B,d,solver.A);
        solver.(['phi',num2str(test)]) = solver.A\solver.b;
        plot(grid.x,solver.(['phi',num2str(test)]),'Parent',axes1,...
                'Marker','.','LineStyle','--','DisplayName',num2str(2^test))
    end
    
    if controller.calc_error
        solver.error(test,1) = norm(solver.(['phip',num2str(test)])-solver.avg_y',1);
        solver.error(test,2) = norm(solver.avg_y,1);
        solver.error(test,3) = mean(grid.delta)*solver.error(test,1)/solver.error(test,2);
        solver.error(test,4) = grid.num_points;
        if controller.iplot
            plot(log(grid.num_points),log(solver.error(test,3)),'Parent',axes3,...
                'Marker','.','LineStyle','--','DisplayName',num2str(2^test))
        end 
    end
end
toc

if solver.natural
    legend1 = legend(axes1,'show');
    set(legend1,'Box','off','Location','Best');
    xlabel(axes1,'x')
    ylabel(axes1,'\phi')
end

if controller.iplot
    legend2 = legend(axes2,'show');
    set(legend2,'Box','off','Location','Best')
    xlabel(axes2,'x')
    ylabel(axes2,'\phi')
    if controller.calc_error
        legend3 = legend(axes3,'show');
        set(legend3,'Box','off','Location','Best')
        plot(log(solver.error(:,4)),log(solver.error(:,3)),'Parent',axes3,'LineStyle','--')
        xlabel(axes3,'log(num points)')
        ylabel(axes3,'log(error)')
    end
end
