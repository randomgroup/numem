function avg_f = averagef(func,grid,average)
    
    if nargin==2
        average = 0;
    end
    if average
        for i = 1:grid.num_points-1
            avg_f(i) = (1.0/grid.delta(i))*integral(func,grid.x(i),grid.x(i+1));
        end
        avg_f(grid.num_points) = avg_f(end);
    else
        for i=1:grid.num_points
            avg_f(i) = func(grid.x(i));
        end
    end
end


