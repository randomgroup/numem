function solution = gauss_jacobi(solver,state,grid)
% function solution = gauss_jacobi(solver,state,grid)
%
% calculated the Gauss elimination for Jacobi iteration

imax = grid.ny;
jmax = grid.nx;

I = 2:imax-1;
J = 2:jmax-1;

dx = grid.delta(1);
dy = grid.delta(2);

u = state.u;
unew = u;

error = zeros(solver.max_iter+1,1);

switch solver.system
    case 'homogeneous'
        if dx==dy
            for iter = 0:solver.max_iter
                unew(I,J) = 0.25*(u(I,J-1) + u(I,J+1) + u(I-1,J) + u(I+1,J));
                error(iter+1) = norm(unew - u,2)/norm(unew,2); 
                u(I,J) = unew(I,J);
                if solver.debug==1
                    pcolor(u)
                end
            end
        else
            dm = (dx^2*dy^2)/(2*(dx^2 + dy^2));
            for iter = 0:solver.max_iter
                unew(I,J) = dm.*((u(I,J-1) + u(I,J+1))./(dx^2)... 
                               + (u(I-1,J) + u(I+1,J))./(dy^2));
                error(iter+1) = norm(unew - u,2)/norm(unew,2); 
                u(I,J) = unew(I,J);
            end
            if solver.debug==1
                pcolor(u)
            end
        end
    case 'heterogeneous'
        if dx==dy
            h = dx^2;
            for iter = 0:solver.max_iter
                unew(I,J) = 0.25*(u(I,J-1) + u(I,J+1) ...
                              + u(I-1,J) + u(I+1,J) - h*state.source(I,J));
                error(iter+1) = norm(unew - u,2)/norm(unew,2); 
                u(I,J) = unew(I,J);
                if solver.debug==1
                    pcolor(u)
                end
            end
        else
            dm = (dx^2*dy^2)/(2*(dx^2 + dy^2));
            for iter = 0:solver.max_iter
                unew(I,J) = dm.*((u(I,J-1) + u(I,J+1))./(dx^2) ... 
                               + (u(I-1,J) + u(I+1,J))./(dy^2) ...
                               - state.source(I,J));
                error(iter+1) = norm(unew - u,2)/norm(unew,2); 
                u(I,J) = unew(I,J);
            end
            if solver.debug==1
                pcolor(u)
            end
        end
    otherwise
        error('-1','solver.system should be either heterogeneous or homogeneous')
end

solution.u = u;
solution.error = error;

