function solution = gauss_seidel_jacobi(solver,state,grid)
% function solution = gauss_jacobi(solver,state,grid)
%
% calculated the Gauss elimination for Jacobi iteration

imax = grid.ny;
jmax = grid.nx;

I = 2:imax-1;
J = 2:jmax-1;

dx = grid.delta(1);
dy = grid.delta(2);

u = state.u;
unew = u;

error = zeros(solver.max_iter+1,1);

switch solver.system
    case 'homogeneous'
        if dx==dy
            for iter = 0:solver.max_iter
                u_old = u;
                for j=2:jmax-1
                    for i=2:imax-1
                        u(i,j) =  0.25*(u(i,j-1) + u(i,j+1) ...
                                        + u(i-1,j) + u(i+1,j));
                    end
                end
                error(iter+1) = norm(u_old - u,2)/norm(u,2);
                if solver.debug==1
                    pcolor(u)
                end
            end
        else
            dm = (dx^2*dy^2)/(2*(dx^2 + dy^2));
            for iter = 0:solver.max_iter            
                u_old = u;
                for j=2:jmax-1
                    for i=2:imax-1
                        u(i,j) =  dm.*((u(i,j-1) + u(i,j+1))/(dx^2) ...
                                        + (u(i-1,j) + u(i+1,j))/(dy^2));
                    end
                end
                error(iter+1) = norm(u_old - u,2)/norm(u,2);
                if solver.debug==1
                    pcolor(u)
                end
            end
        end
    case 'heterogeneous'
        if dx==dy
            h = dx^2;
            for iter = 0:solver.max_iter
                u_old = u;
                for j=2:jmax-1
                    for i=2:imax-1
                        u(i,j) =  0.25*(u(i,j-1) + u(i,j+1) ...
                                        + u(i-1,j) + u(i+1,j) ...
                                        - h*state.source(i,j));
                    end
                end
                error(iter+1) = norm(u_old - u,2)/norm(u,2);
                if solver.debug==1
                    pcolor(u)
                end
            end
        else
            dm = (dx^2*dy^2)/(2*(dx^2 + dy^2));
            for iter = 0:solver.max_iter
                u_old = u;
                for j=2:jmax-1
                    for i=2:imax-1
                        unew(i,j) = dm.*((u(i,j-1) + u(i,j+1))./(dx^2) ... 
                                        + (u(i-1,j) + u(i+1,j))./(dy^2) ...
                                        - state.source(i,j));
                    end
                end
                error(iter+1) = norm(u_old - u,2)/norm(u,2); 
            end
            if solver.debug==1
                pcolor(u)
            end
        end
    otherwise
        error('-1','solver.system should be either heterogeneous or homogeneous')
end

solution.u = u;
solution.error = error;