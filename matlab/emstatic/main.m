clear all
%% setup problem
% solver paramenter
solver.tolerance = 1e-4;
solver.max_iter = 40000;
solver.method = 'gauss_seidel';
solver.system = 'homogeneous';
solver.bc_type = 'Dirichlet';
solver.with_error_test = 1;
solver.plot = 1;
solver.debug = 0;

% set grid
grid.dimx = [0,8];
grid.dimy = [0,4];


if solver.plot
    glog = figure;
end

for test=1:6
    tic
    % set the grid size (dx=dy)
    grid.delta = ones(1,2)*2^(-test);
    
    % calculate number of points
    grid.s = [grid.dimx(2)-grid.dimx(1),grid.dimy(2)-grid.dimy(1)];
    grid.num_points = floor(grid.s/grid.delta);
    
    %grid.delta = grid.s./[grid.nx - 1,grid.ny - 1];
    %grid.delta = [grid.s(1)/(grid.nx-1),grid.s(1)/(grid.nx-1)];
    %grid.ny = size(0:grid.delta(1):4);
    
    % calculate axis
    grid.x = grid.dimx(1):grid.delta(1):grid.dimx(2);
    grid.y = grid.dimy(1):grid.delta(2):grid.dimy(2);
    
    grid.nx = size(grid.x,2);
    grid.ny = size(grid.y,2);

    [X,Y] = meshgrid(grid.x,grid.y);
    % set boundary conditions
    ubc.t =  zeros(grid.nx,1);
    ubc.b =  zeros(grid.nx,1);
    ubc.l = 5*ones(grid.ny,1);
    ubc.r =  zeros(grid.ny,1);
    
    %% prolegomena
    
    % create u
    state.u = randn(grid.ny,grid.nx)*5;
    
    % apply boundary conditions
    
    state.u(1,:) = ubc.t;         % top
    state.u(grid.ny,:) = ubc.b;   % bottom
    state.u(:,1) = ubc.l;         % left
    state.u(:,grid.nx) = ubc.r;   % right
    state.ubc = ubc;
    
    state.source = zeros(grid.nx*grid.ny,1);
    
    %% calculation
    
    % potential
    switch solver.method
        case 'gauss' || 'jacobi' || 'gauss jacobi'
            solution = gauss_jacobi(solver,state,grid);
        case 'gauss_seidel' || 'seidel'
            solution = gauss_seidel_jacobi(solver,state,grid);
        case 'backslash' || 'direct inverse'
            solution = laplace_implicit(solver,state,grid);
        otherwise
            error(['Method not implemented, only possibilities are:\n',...
                '''gaus'', ''gauss_seidel'' and ''inverse'''])
    end
    
    solution.grid = grid;
    
    % calculate potential and error
    solution.V = potential(X,Y,5,[8,4],10);
    if solver.with_error_test = 1
        solution.u_error(test,1) = grid.delta(1);
        solution.u_error(test,2) = grid.delta(2);
        solution.u_error(test,3) = (grid.delta(1)*grid.detal(2)).*... 
                                    norm(solution.u - solution.V,1)./...
                                    norm(solution.V,1);
    end
    
    % calculate gradient of potential, i.e. field
    F = gradientf(solution.u,solution.grid);
    
    if solver.plot
        h = figure;
        subplot(2,1,1)
        pcolor(X,Y,solution.u),  colorbar, shading interp
        title('Solution')
        xlabel('x')
        ylabel('y')
        subplot(2,1,2)
        loglog(0:solver.max_iter,solution.error)
        xlabel('Log(iter)')
        ylabel('Log(error)')
        print(h,'-depsc2',['solution',num2str(test),'.eps'])
        save(['solution',num2str(test)],'solution')
    
        h1 = figure;
        contour(X,Y,solution.u);
        colorbar
        hold on
        quiver(X(1:floor(1.5^test):end-1,1:floor(1.5^test):end-1)+grid.delta(1),...
            Y(1:floor(1.5^test):end-1,1:floor(1.5^test):end-1)+grid.delta(2),...
            -F.x(1:floor(1.5^test):end-1,1:floor(1.5^test):end-1),...
            -F.y(1:floor(1.5^test):end-1,1:floor(1.5^test):end-1))
        xlabel('x')
        ylabel('y')
        hold off
        print(h1,'-depsc2',['Fsolution',num2str(test),'.eps'])
    
        figure(glog)
        hold on
        plot(solution.u_error(test,1),solution.u_error(test,3),'r.','MarkerSize',15)
    end
end

save('results','fverror','solution')

if solver.plot
    figure(glog)
    hold on
    xlabel('log delta')
    ylabel('log(error) (a.u.)')
    print(glog,'-depsc2',['Ferrorlog',num2str(test),'.eps'])
end




