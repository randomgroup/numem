function gradf = gradientf(scalar_field,gridf)

if nargin==1
    gridf = 0;
end
order = num2str(length(size(scalar_field)));

switch order
    case '2'
        if isfield(gridf,'nx') && isfield(gridf,'ny')
            imax = gridf.ny;
            jmax = gridf.nx;
 
            dx = gridf.delta(1);
            dy = gridf.delta(2);
        else
            imax = size(scalar_field,1);
            jmax = size(scalar_field,2);
 
            dx = 1;
            dy = 1;
        end
        
        I = 2:imax;
        J = 2:jmax;
        gradf.x = zeros(imax,jmax);
        gradf.y = gradf.x;
        gradf.x(:,J-1) = (scalar_field(:,J) - scalar_field(:,J-1))./dx;
        gradf.y(I-1,:) = (scalar_field(I,:) - scalar_field(I-1,:))./dy;
        
    case '3'
        if isfield(gridf,'nx') && isfield(gridf,'ny') && isfield(gridf,'nz')
            imax = gridf.ny;
            jmax = gridf.nx;
            kmax = gridf.nz;
            
            dx = gridf.delta(1);
            dy = gridf.delta(2);
            dz = gridf.delta(3);
        else
            imax = size(scalar_field,1);
            jmax = size(scalar_field,2);
            kmax = size(scalar_field,3);
            dx = 1;
            dy = 1;
            dz = 1;
        end
        
        I = 2:imax;
        J = 2:jmax;
        K = 2:kmax;
        
        gradf.x = zeros(imax,jmax,kmax);
        gradf.y = gradf.x;
        gradf.z = gradf.z;
        
        gradf.x(:,J-1,:) = (scalar_field(:,J,:) - scalar_field(:,J-1,:))./dx;
        gradf.y(I-1,:,:) = (scalar_field(I,:,:) - scalar_field(I-1,:,:))./dy;
        gradf.z(:,:,K-1) = (scalar_field(:,:,K) - scalar_field(:,:,K-1))./dz;
    otherwise
        error('Only 2D and 3D scalar field gradient is implemented');
end