function V = potential(X,Y,Vo,dim,order)

n  = 1:2:order;
np = n*pi;

a = dim(1);
b = dim(2);

V  = zeros(size(X));

for i = 1:size(n,2)
    V = V + (sinh(np(i).*((a-X)./b))./(n(i)*sinh(np(i)*a/b))).*sin((np(i).*Y)./b);
end

V = 4*Vo*V/pi;